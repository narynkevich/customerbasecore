﻿using CustomerBaseCore.Models;
using CustomerBaseCore.ModelsView;
using System;
using System.Collections.Generic;
using System.Data;

using System.Linq;
using System.Threading.Tasks;

namespace CustomerBaseCore.ContextDB
{
    public class ContextDBLexicon : ContextDB
    {

       
        public ContextDBLexicon(IDbConnection connection) : base(connection)
        {
        }



        /// <summary>
        /// Метод: Получить список объектов + Список кураторов
        /// </summary>
        /// <returns></returns>
        public List<Customers> GetCustomers()
        {
            var customersList = new List<Customers>();

            // Запрос к БД
            var sqlQuery = @"
                       select
                         ct.IDCUSTOMER,
                         ct.CUSTOMER_NAME,
                         iif(ct.ADDRESS is null, '', ct.ADDRESS) as ADDRESS,
                         iif(ct.ADDITIONAL_INFO is null, '', ct.ADDITIONAL_INFO) as ADDITIONAL_INFO,
                         iif(vs.IDVISITOR is null, -1, vs.IDVISITOR) as IDVISITOR,
                         iif(vs.VISITOR_NAME is null, '', vs.VISITOR_NAME) as VISITOR_NAME,
                         iif(vs.VISITOR_POSITION is null, '', vs.VISITOR_POSITION) as VISITOR_POSITION,
                         iif(vs.VISITOR_CONTACT is null, '', vs.VISITOR_CONTACT) as VISITOR_CONTACT
                       from CUSTOMERS ct
                         left join  VISITORS vs on ct.CURATOR_ID = vs.IDVISITOR
                       order by CUSTOMER_NAME  ";


            // using (IDbConnection cnn = new FbConnection(_appSettings.Value.ConnectionStrings.DefaultConnection))
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sqlQuery;

                _connection.Open();

                var reader = command.ExecuteReader();


                // reader.
                while (reader.Read()) // построчное считывание данных
                {
                    customersList.Add(
                        new Customers
                        {
                            Id = (int)reader["IDCUSTOMER"],
                            CustomerName = (string)reader["CUSTOMER_NAME"],
                            Address = (string)reader[
                            "ADDRESS"], // reader["ADDRESS"]  DBNull ? string.Empty :  (string)reader["ADDRESS"]
                            AdditionalInfo = (string)reader["ADDITIONAL_INFO"],
                            CuratorId = (int)reader["IDVISITOR"],
                            Visitors = new Visitors
                            {
                                Id = (int)reader["IDVISITOR"],
                                VisitorName = (string)reader["VISITOR_NAME"],
                                VisitorPosition = (string)reader["VISITOR_POSITION"],
                                VisitorContact = (string)reader["VISITOR_CONTACT"]
                            }
                        });
                }

                _connection.Close();
            }

            return customersList;


        }






        /// <summary>
        /// Метод: Получить список кураторов объектов (таблица VISITORS)
        /// </summary>
        /// <returns></returns>
        public List<Visitors> GetVistors()
        {

            var visitorList = new List<Visitors>();

            // Текст запроса к БД список кураторов         
            var sqlQuery = @"
                        select v.IDVISITOR, v.VISITOR_NAME from VISITORS v
                        order by v.VISITOR_NAME";


            // using (IDbConnection cnn = new FbConnection(_appSettings.Value.ConnectionStrings.DefaultConnection))
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sqlQuery;

                _connection.Open();

                var reader = command.ExecuteReader();

                // reader.
                while (reader.Read()) // построчное считывание данных
                {
                    visitorList.Add(
                        new Visitors((string)reader["VISITOR_NAME"])
                        {
                            Id = (int)reader["IDVISITOR"]
                        });
                }

                _connection.Close();
            }

            return visitorList;
        }






        /// <summary>
        /// Метод: Получить список видов посещений (таблица VISIT_TYPES)
        /// </summary>
        /// <returns></returns>
        public List<VisitTypes> GetVisitTypes()
        {

            var visitTypeList = new List<VisitTypes>();

            // Текст запроса к БД список видов посещений         
            var sqlQuery = @"
                        select vt.IDTYPE, vt.TYPE_NAME from VISIT_TYPES vt
                        order by vt.SORT_ORDER";


            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sqlQuery;

                _connection.Open();

                var reader = command.ExecuteReader();

                // reader.
                while (reader.Read()) // построчное считывание данных
                {
                    visitTypeList.Add(
                        new VisitTypes()
                        {
                            Id = (int)reader["IDTYPE"],
                            TypeName = (string)reader["TYPE_NAME"]
                        });
                }

                _connection.Close();
            }

            return visitTypeList;
        }






        /// <summary>
        /// Метод: Получить список видов выполненной работы (таблица TASKS)
        /// </summary>
        /// <returns></returns>
        public List<Tasks> GetTasks()
        {

            var taskList = new List<Tasks>();

            // Текст запроса к БД: список - вид выполненной работы        
            var sqlQuery = @"
                        select t.IDTASK, t.TASK_NAME from TASKS t
                        order by t.TASK_NAME";



            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sqlQuery;

                _connection.Open();

                var reader = command.ExecuteReader();

                // reader.
                while (reader.Read()) // построчное считывание данных
                {
                    taskList.Add(
                        new Tasks()
                        {
                            Id = (int)reader["IDTASK"],
                            TaskName = (string)reader["TASK_NAME"]
                        });
                }

                _connection.Close();
            }

            return taskList;
        }




    }
}
