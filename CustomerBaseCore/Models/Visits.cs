﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CustomerBaseCore.Models
{
    /// <summary>
    /// Модель для таблицы VISITS (факт посещения)
    /// </summary>
    [Table("VISITS")]
    public class Visits
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column("IDVISIT")]
        public int Id { get; set; }


        [Column("CUSTOMER_ID")]
        [ForeignKey("FK_VISITS")]
        public int? CustomerID { get; set; }
        // public Customers Customers;


        private DateTime _date;

        /// <summary>
        /// Дата сопровождения
        /// </summary>
        [Required]
        [Column("VISIT_DATE")]
        public DateTime VistDate
        {
            get => _date;
            set => _date = value.Date;
        }




        [Column("VISIT_TYPE")]
        [ForeignKey("FK_VISITS_2")]
        public int? VistiTypeID { get; set; }
        // public VisitTypes VisitTypes;
    }
}
