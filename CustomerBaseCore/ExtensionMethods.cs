﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections;
using System.Data;
using System.Text.Encodings.Web;

namespace CustomerBaseCore
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Метод расширения для IDbCommand. Добавление параметра для комманды запроса
        /// </summary>
        /// <param name="command">объект команды</param>
        /// <param name="name">наименование праметра: "@parameter"</param>
        /// <param name="value">Значение передаваемое в параметр запроса</param>
        /// <param name="type">Тип параметра</param>
        public static void AddParameter(this IDbCommand command, string name, object value, DbType type)
        {
            var dbParameter = command.CreateParameter();
            dbParameter.DbType = type;
            dbParameter.ParameterName = name;
            dbParameter.Value = value;
            command.Parameters.Add(dbParameter);
        }





        /// <summary>
        /// Extension method for IHtmlHelper. Rendering for input & datalist tags. Adds work with ID item (data-id) for datalist. + CSS + JavaScript. 
        /// Depends on "Bootstrap" and site.js methods
        /// </summary>
        /// <param name="listItems">The list of objects with the "Id ..." and "Name ..." fields</param>
        /// <param name="inputAttrName">Required. Name for attribute “name” for "model binding". </param>
        /// <param name="inputAttrDataId">Value (id of selected item) for attribute “value” for "model binding". Default: "-1"</param>
        /// <param name="listPropId">The property name of the object in the List for item-id. Default: "Id" (case sensitive)</param>
        /// <param name="listPropName">The property name of the object in the List for item name (datalist: option). Default: "Name" or containing "*name*". (case insensitive)</param>
        /// <param name="htmlAttrInput"> html attributes for tag "input" </param>
        /// <returns></returns>required
        public static HtmlString InputDataListHelper(
            this IHtmlHelper html, string inputAttrName, IList listItems = null, string inputAttrDataId = "-1",
            string listPropId = "Id", string listPropName = null, object htmlAttrInput = null, object htmlAttrButton = null)
        {
            var writer = new System.IO.StringWriter();
            var htmlAttrInputType = htmlAttrInput?.GetType();

            if (listItems == null & htmlAttrInputType?.GetProperty("list")?.Name == null)
                return new HtmlString(
                    $"<font color=red>One of the parameters (\"{nameof(listItems)}\" or \"{nameof(htmlAttrInput)}: " +
                    "new {...@list = \"idDataList\"...}\") must have value!</font>");


            // Flag - get <datalist> from "IList" (true) 
            //        or from "htmlAttributes: new {...@list = "idDataList"...}"
            var bylistItems = (htmlAttrInputType?.GetProperty("list")?.Name == null);

            if (bylistItems)
            {
                Type listType = listItems.GetType();
                Type itemType = listType.GetGenericArguments()[0]; // Get type of item in List (List<T>: get type of <T> )

                if (listPropName == null)
                {
                    // Search property "Name" (exact match)
                    foreach (var prop in itemType.GetProperties())
                    {
                        if (prop.Name.ToUpper() == "NAME")
                        {
                            listPropName = prop.Name;
                            break;
                        }
                    }

                    if (listPropName == null)
                    {
                        foreach (var prop in itemType.GetProperties())
                        {
                            if (prop.Name.ToUpper().Contains("NAME"))
                            {
                                listPropName = prop.Name;
                                break;
                            }
                        }
                    }
                    if (listPropName == null)
                        listPropName = "...Name";
                }

                //Check on existing poperty ..Id" or "...Name" in IList
                if (itemType.GetProperty(listPropId)?.Name == null)
                    return new HtmlString($"<font color=red>Poperty \"{listPropId}\" not found in the obect \"IList\" (parameter \"{nameof(listPropId)}\")!</font>"); ;

                if (itemType.GetProperty(listPropName)?.Name == null)
                    return new HtmlString($"</br><font color=red>Poperty \"{listPropName}\" not found in the obect \"IList\" (parameter \"{nameof(listPropName)}\")!</font></br>"); ;
            }



            // tag <input list="...">...</input> (visible, for <datalist>)
            var tagInput = new TagBuilder("input");
            // set attributes
            if (htmlAttrInputType != null)
                foreach (var prop in htmlAttrInputType.GetProperties())
                {
                    var attrName = prop.Name.ToLower();
                    var attrValue = htmlAttrInputType.GetProperty(prop.Name).GetValue(htmlAttrInput)?.ToString();

                    if (attrName == "id")
                        tagInput.GenerateId(attrValue, "_");
                    else if (attrName == "class")
                        tagInput.AddCssClass(attrValue);
                    else
                        tagInput.Attributes.Add(attrName, attrValue);
                }
            // check value for attribute "id"
            string inputAttrId;
            tagInput.Attributes.TryGetValue("id", out inputAttrId);
            if (inputAttrId == null)
            {
                inputAttrId = Guid.NewGuid().ToString();
                tagInput.GenerateId(inputAttrId, "_");
                inputAttrId = tagInput.Attributes["id"];
            }
            tagInput.AddCssClass("input-dl-hlpr");
            // add Bootstrap class
            tagInput.AddCssClass("form-control");

            // action onchange: control edited value + set data-id value into hiddenInput
            if (!tagInput.Attributes.ContainsKey("onchange"))
                tagInput.Attributes.Add("onchange", $"InputDataListOnChangeHendler('{inputAttrId}')");




            // tag <datalist>...</datalist>
            TagBuilder tagDatalist = new TagBuilder("datalist");
            if (!tagInput.Attributes.ContainsKey("list"))
            {
                var datalistId = "datalist_" + inputAttrId;
                tagDatalist.GenerateId(datalistId, "_");
                datalistId = tagDatalist.Attributes["id"];
                tagInput.Attributes.Add("list", datalistId);
                // fill list of <option>
                foreach (var obj in listItems)
                {
                    Type objType = obj.GetType();
                    var id = objType.GetProperty(listPropId).GetValue(obj).ToString();
                    var name = objType.GetProperty(listPropName).GetValue(obj).ToString();

                    var tagOption = new TagBuilder("option");
                    tagOption.Attributes.Add("data-id", id);
                    tagOption.Attributes.Add("value", name);

                    tagDatalist.InnerHtml.AppendHtml(tagOption);

                    // fill in tagInput (attribute "value")
                    if (id == inputAttrDataId)
                        if (!tagInput.Attributes.ContainsKey("value"))
                            tagInput.Attributes.Add("value", name);
                }

                tagDatalist.WriteTo(writer, HtmlEncoder.Default);
            }



            // div for autowidth
            TagBuilder divInputAutoWidth = new TagBuilder("div");
            divInputAutoWidth.GenerateId("autowidth_" + inputAttrId, "_");
            divInputAutoWidth.Attributes.Add("style", "position: absolute; visibility: hidden; white-space: nowrap;");
            divInputAutoWidth.WriteTo(writer, HtmlEncoder.Default);


            // div for min-width
            TagBuilder divInputMinWidth = new TagBuilder("div");
            divInputMinWidth.GenerateId("minwidth_" + inputAttrId, "_");
            divInputMinWidth.Attributes.Add("style", "position: absolute; visibility: hidden;");
            divInputMinWidth.WriteTo(writer, HtmlEncoder.Default);


            // tag <input type="hidden"></input> (hidden, for "model binding")
            var tagInputHidden = new TagBuilder("input");
            tagInputHidden.GenerateId("hidden_" + inputAttrId, "_");

            tagInputHidden.Attributes.Add("type", "hidden");
            tagInputHidden.Attributes.Add("name", inputAttrName);
            tagInputHidden.Attributes.Add("value", inputAttrDataId);
            tagInputHidden.WriteTo(writer, HtmlEncoder.Default);


            // main tag <div>...</div>
            var tagDivInputGroup = new TagBuilder("div");
            tagDivInputGroup.GenerateId("mainDiv_" + inputAttrId, "_");
            tagDivInputGroup.AddCssClass("input-group");


            tagDivInputGroup.InnerHtml.AppendHtml(tagInput);



            // tag <span>...</span>
            var tagDivButton = new TagBuilder("div");
            tagDivButton.AddCssClass("input-group-append");




            // tag <button>...</button>
            var tagButton = new TagBuilder("button");
            // set attributes
            var htmlAttrButtonType = htmlAttrButton?.GetType();
            if (htmlAttrButtonType != null)
                foreach (var prop in htmlAttrButtonType.GetProperties())
                {
                    var attrName = prop.Name.ToLower();
                    var attrValue = htmlAttrButtonType.GetProperty(prop.Name).GetValue(htmlAttrButton)?.ToString();

                    if (attrName == "id" || attrName == "type")
                        continue; // exclude "id", "type". The "id", "type" attributes must be set directly.
                    else if (attrName == "class")
                        tagButton.AddCssClass(attrValue);
                    else
                        tagButton.Attributes.Add(attrName, attrValue);
                }
            tagButton.GenerateId("btn_" + inputAttrId, "_");
            tagButton.AddCssClass("clr-selected");
            tagButton.AddCssClass("btn");
            tagButton.Attributes.Add("type", "button");
            tagButton.Attributes.Add("tabindex", "-1");
            tagButton.InnerHtml.AppendHtml("&nbsp;");

            // action onclick button: clear selected value
            tagButton.Attributes.Add("onclick", $"ClearButtonOnClickHendler('{inputAttrId}')");



            // prepare the final html code to return
            tagDivButton.InnerHtml.AppendHtml(tagButton);
            tagDivInputGroup.InnerHtml.AppendHtml(tagDivButton);

            tagDivInputGroup.WriteTo(writer, HtmlEncoder.Default);

            return new HtmlString(writer.ToString());
        }

    }
}
