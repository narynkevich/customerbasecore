﻿/* Source: 
 * https://gist.github.com/wellwind/a5a10222ad691d72a11c#file-1-draggable-modal-js
 * */


$(document).ready(function () {
    $('.modal.draggable>.modal-dialog').draggable({
        cursor: 'move' ,
        handle: '.modal-header'
    });
    $('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');
});