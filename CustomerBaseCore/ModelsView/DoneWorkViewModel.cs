﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CustomerBaseCore.Models;
using Microsoft.AspNetCore.Razor.Language;

namespace CustomerBaseCore.ModelsView
{
    /// <summary>
    /// Модель для View - Отчет о проделанной работе 
    /// </summary>
    public class DoneWorkViewModel
    {
        // Входные параметры для отчета (из View)
        [Required]
        public DateTime DateFrom { get; set; }

        [Required]
        public DateTime DateTill { get; set; }

        public int? ParamVisitorId { get; set; } = -1;

        public int? ParamCustomerId { get; set; } = -1;

        /// <summary>
        /// Display comments without time. "false" - display time; "true" - hide time;
        /// </summary>
        public bool WithoutTime { get; set; } 


        // Список данных, возвращемые БД
        public List<DoneWorkRow> ProgressReportList { get; set; }
        public List<Visitors> VisitorList { get; set; }

        public List<Customers> CustomersList { get; set; }

    }

    /// <summary>
    /// Вспомогательная модель для запроса - хранит значения для колонок для одной строки запроса
    /// </summary>
    public class DoneWorkRow
    {
        /// <summary>
        /// Visit id for support fact in row
        /// </summary>
        public int VisitId { get; set; }

        private readonly string[] DayOfWeekToString = { "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" };
        public DateTime VisitDate { get; set; }

        public string VisitDateString => VisitDate.ToString("yyyy/MM/dd") + " " + DayOfWeekToString[(int)VisitDate.DayOfWeek];

        public string TypeName { get; set; } = "";
        public string CustomerName { get; set; } = "";
        public string AllVisitors { get; set; } = "";

        /// <summary>
        /// Display comments without time. "false" - display time; "true" - hide time;
        /// </summary>
        public bool WithoutTime { get; set; }

        public List<CompletedJob> CompletedJobs { get; set; }


        /// <summary>
        ///  A sign that a new support protocol has just been created (true - is added new row)
        /// </summary>
        public bool IsNewRow { get; set; }

    }
}
