﻿using System.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FirebirdSql.Data.FirebirdClient;
using CustomerBaseCore.ContextDB;

namespace CustomerBaseCore
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            AppConfiguration = configuration;
        }


        public IConfiguration AppConfiguration { get; set; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Настройка параметров и DI
            services.AddOptions();

            // создание объекта AppSettings по ключам из конфигурации
            services.Configure<AppSettings>(AppConfiguration);

            // добавить в сервисы (DI) подключение к БД (FireBird)
            services.AddTransient<IDbConnection>(
                db => new FbConnection(AppConfiguration.GetConnectionString("DefaultConnection")));

            // добавить в сервисы (DI) - классы контекста БД
            services.AddTransient<ContextDBLexicon>();
            services.AddTransient<ContextDBDoneWork>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


        }




        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Если приложение находится в разработке 
            // env.EnvironmentName = EnvironmentName.Production;
            if (env.IsDevelopment())
            {
                // Вывводить сообщение об Исключениях на web-страницу
                app.UseDeveloperExceptionPage();
            }



            // Обработка ошибок HTTP
            app.UseStatusCodePages();


            // Подключить (в область видимости) статические файлы ../wwwroot/...
            app.UseStaticFiles();



            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Reports}/{action=Index}/{id?}");
            });

        }
    }
}
